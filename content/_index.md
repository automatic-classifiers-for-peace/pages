## ACfP Goals
The `ACfP` repository is aiming to be a `space/tools/platform` that can allow for the sharing of work between different researchers and organisations that can collaborate in a way that is:

- publicly visible
- other people/organisations can easily join in
- allows for the sharing of data in a way that fits the sensitivity of the data
- allows for the formalisation of shared code that can lead to high-quality libraries
- work towards products/classification models could be publicly used