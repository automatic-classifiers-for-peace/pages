---
title: About ACfP
subtitle: Helping Peacebuilders, build a better world
comments: false
---

The automatic classifiers for peace group is an open-source community of researchers, organisations and individuals that are interested in aiding social media analysis through the automatic classification of social media discourse. This repository contains the experiments, documentation, tutorials and source code that the group has developed.
We build 
- CLassiefiers
- Data Hub
- Annotation guidelines



### How we are established

[Build up](https://howtobuildup.org/) and [Datavaluepeople](https://datavaluepeople.com/) have ...