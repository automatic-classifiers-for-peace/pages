---
title: Who are we
subtitle: We are researchers, organisations and individuals that are interested in aiding social media analysis through the automatic classification of social media discourse. 
comments: false
tags: ["Team"]
---

## Teams
The following individuals constitute the members of ACfP, encompassing professionals from research, academia, industry, and volunteers.

| Name &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  | Company &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;| Photo |
| -------- | ------- | ------ |
| Helena Puig Larrauri  |[Build Up](https://howtobuildup.org/about-build-up/team/helena-puig-larrauri/)    | ![Helena](https://howtobuildup.org/wp-content/uploads/2022/07/Helena2022_CME_5721_sq.jpg) | 
| Benjamin Cerigo | [Datavaluepeople](https://datavaluepeople.com/people/) | ![Ben](https://datavaluepeople.com/static/61afdc8c84867f55f30af5579df6ff0d/benjamin.gif) |
| Seid Muhie Yimam    | [HCDS-UHH](https://www.hcds.uni-hamburg.de/en/hcds/head-hcds/yimam.html)    | ![Seid](https://datascience-hamburg.org/images/03_personen/seidmuhie_yimam.jpg#joomlaImage://local-images/03_personen/seidmuhie_yimam.jpg?width=600&height=600) | 
| Andrew Sutjahjo| [Datavaluepeople](https://datavaluepeople.com/people/) | ![Andrew](https://datavaluepeople.com/static/2bf19c1c5f5b558a789c614ad19b5c85/andrew.gif) |